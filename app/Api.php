<?php

namespace App;

use GuzzleHttp\Client;

class Api
{
    protected $api_addr = 'https://shopogolic.net/api/';
    protected $token;
    protected $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => $this->api_addr]);
        $this->auth_header = 'Basic ' . base64_encode(config('app.shopogolic_api_key').':');
    }

    public function newUser() {

    }

    public function newAddress()
    {
        // code...
    }

    public function getAddresses($user_id, $page = 1)
    {
        $res = $this->client->request('GET', 'addresses', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => $this->auth_header,
            ],
            'body' => json_encode([
                    'filter' => ['user_id' => $user_id],
                    'page' => $page
                ])
        ]);
        if ('200' == $res->getStatusCode()) {
            return json_decode((string)$res->getBody());
        }
        return false;
    }

    public function getParcel($id = false)
    {
        // code...
    }

    public function getOrders($value='')
    {
        // code...
    }
}
